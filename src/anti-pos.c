/*
 *  Project: Anti-Pos
 *	File: pos.c
 *	
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date: 09/11/2020
 *
 */

#define VERSION "1.0.0"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *help[] = {
	"     anti-pos "VERSION,
	"",
	"Usage: anti-pos [options...]",
	"",
	"Options:",
	"--ascii         : ASCII characthers only",
	"--help          : Prints this message",
	"--version       : Prints the version number",
	"-d <delimiter>  : Sets the POS-tag delimitier",
	"-f <file>       : Set input file",
	"-o <file>       : Set output file"
};

int main(int argc, char** argv)
{
	char delimiter = '_';
	char ascii = 0;

	for(int i = 0; i < argc; ++i){
		if(!strcmp(argv[i], "--version")){
			printf("anti-pos %s\n", VERSION);
			return 0;
		}
		else if(!strcmp(argv[i], "--help")){
			for(int j = 0; j < sizeof(help) / sizeof(help[0]); ++j)
				printf("%s\n", help[j]);
			return 0;
		}
		else if(!strcmp(argv[i], "--ascii")){
			ascii = 1;
		}
		else if(!strcmp(argv[i], "-d")){
			i++;
			if(i == argc){
				printf("Error: Missing argument for -d\n");
				return 1;
			}
			delimiter = argv[i][0];
		}
		else if (!strcmp(argv[i], "-f")) {
			i++;
			if (i == argc) {
				fprintf(stderr, "Error: No file provided for flag -f\n");
				exit(1);			
			}

			FILE *in = freopen(argv[i], "r", stdin);
			if (!in) {
				fprintf(stderr, "Error: Failed to open input stream %s\n", argv[i]);
				exit(1);
			}
		}
		else if (!strcmp(argv[i], "-o")) {
			i++;
			if (i == argc) {
				fprintf(stderr, "Error: No file provided for flag -o\n");
				exit(1);			
			}

			FILE *out = freopen(argv[i], "w+", stdout);
			if (!out) {
				fprintf(stderr, "Error: Failed to open output stream %s\n", argv[i]);
				exit(1);
			}
		}	
	}

	char c;
	int in_pos = 0;
	while((c = getchar()) != EOF){
		if(c == delimiter)
			in_pos = 1;
		if(c == ' ' || c == '\t' || c == '\r' || c == '\n')
			in_pos = 0;
		if(!in_pos && (!ascii || (ascii && (isalnum(c) || c ==	' ' || c == '\n' || c == '\r' || c == '\t'))))
			putchar(c);
	}
	return 0;
}

